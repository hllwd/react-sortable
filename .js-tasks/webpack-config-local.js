'use strict'

var path = require('path')
var configCommon = require('./webpack-config-common.js')
var extend = require('lodash/object/extend')

module.exports = extend({}, configCommon, {
    devtool: '#source-map'
})