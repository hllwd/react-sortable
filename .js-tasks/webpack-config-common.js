'use strict'

var webpack = require('webpack')
var path = require('path')

module.exports = {
    entry: path.resolve('web_modules/main.jsx'),
    output: {
        filename: 'main.js',
        path: path.resolve('public/js'),
        publicPath: '/public/js/'
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        root: [
            path.resolve('web_modules')
        ],
        loaders: [
            // es6 / jsx
            {
                test: /\.jsx?$/,
                loader: 'babel?stage=0&optional=runtime',
                exclude: /node_modules/
            }
        ]
    }
}