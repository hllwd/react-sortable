'use strict'

var path = require('path')
var configCommon = require('./webpack-config-common.js')
var extend = require('lodash/object/extend')

module.exports = extend({}, configCommon, {
    // If you pass an array: All modules are loaded upon startup. The last one is exported.
    entry: ['webpack/hot/dev-server', configCommon.entry],
    output: extend({}, configCommon.output, {
        publicPath: 'http://localhost:3021/public/js/'
    }),
    devtool: '#source-map',
    devServer: {
        inline: true,
        port: 3021
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            loaders: ['react-hot', 'babel?stage=0&optional=runtime'],
            exclude: path.resolve('node_modules')
        }]
    }
})