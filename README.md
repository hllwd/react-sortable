# React sortable

## Installation

Le projet requiert l'installation de redis (et son démarrage)

Sur OSX :

    brew install redis
    redis-server /usr/local/etc/redis.conf

Sur Linux :

    sudo apt-get install redis

    
Une fois la base démarrée, on peut y mettre les données par défaut : 
    
    npm run setup-db
    
Il faut ensuite installer les packages JavaScript : 
    
    npm install
    
    
## Développement
    
Démarrer votre serveur node :      
    
    npm run start
    
Puis, lancer le watcher sur vos sources js : 
    
    npm run local
    
Puis ouvrez la [page d'accueil] 
        
        
[page d'accueil]: http://localhost:3022        