var express = require('express')
var app = express()
var server = require('http').createServer(app)
var io = require('socket.io').listen(server)
// we create a namespace to avoid webpack dev server https://github.com/webpack/webpack-dev-server/issues/76
var nsp = io.of('/article')
var http = require('http')
var path = require('path')
var redis = require('./db/client.js')

app.set('port', 3022)
app.use(express.static(path.resolve('public')))

app.get('/test', function(req, res){
    res.sendFile(path.resolve('views/test.html'))
})

app.get('/', function(req, res){
    res.sendFile(path.resolve('views/index.html'))
})

nsp.on('connection', function(socket){


    console.log('new user conncted')
    redis.get('articles').then(function(articles){
        socket.emit('articles', JSON.parse(articles))
    })

    socket.on('update articles', function(articles){
        redis.set('articles', JSON.stringify(articles))
        // envoyer à tout le monde
        socket.broadcast.emit('articles', articles)
    })
})



server.listen(app.get('port'), function(){
    console.log('listening on *:3022');
})