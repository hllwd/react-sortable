var redis = require('./client.js')

var defaultArticles = [{
    title: 'article1',
    id: '0'
},{
    title: 'article2',
    id: '1'
},{
    title: 'article3',
    id: '2'
}]

redis.set('articles', JSON.stringify(defaultArticles))
redis.get('articles').then( function(result){
    console.log(result)
    process.exit(0)
} )