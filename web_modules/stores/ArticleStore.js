import { Store } from 'flummox'
import Immutable from 'immutable'
import socket from '../io/socket.js'

export default class ArticleStore extends Store {

    constructor(flux){
        super()
        // l'état d'un store flummox doit toujours être dans this.state
        this.state = {
            articles: Immutable.fromJS([])
        }

        // on récupère les actions relatives à l'entité Article grâce à la clé fournie
        // dans l'implémentation du dispatcher
        const articleActions = flux.getActions('Article')

        // on s'abonne à l'action asynchrone
        // les 3 derniers paramètres correspondent à begin, success, failure du traitemnt asynchrone
        // on ne précise donc ici que le 3è param
        this.registerAsync( articleActions.retrieveFromServer, null, this.handleRetrieveFromServer, null )
        // articleActions.retrieveFromServer()
        // on souscrit à l'action de récupération des données via socket
        this.register( articleActions.retrieveFromSocket, this.handleRetrieveFromSocket )
        // on intervertit les éléments suivants
        this.register( articleActions.move, this.handleMove )

    }

    // une fois les articles récupérés du serveur, on set simplement le state, à la manière d'un composant react
    handleRetrieveFromServer(articles){
        this.setState({
            articles: Immutable.fromJS(articles)
        })
    }

    handleRetrieveFromSocket(articles){
        this.setState({
            articles: Immutable.fromJS(articles)
        })
    }

    handleMove(involvedArticles){
        let {draggedArticle, currentArticle} = involvedArticles
        let { articles } = this.state

        let draggedInd = articles.indexOf( draggedArticle )
        let currentInd = articles.indexOf( currentArticle )

        let newArticles = articles.splice(draggedInd, 1).splice(currentInd, 0, draggedArticle)

        socket.emit('update articles', newArticles.toJS())

        this.setState({
            articles: newArticles
        })

    }

}