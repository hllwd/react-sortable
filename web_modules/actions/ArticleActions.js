import { Actions } from 'flummox'
import getArticles from '../services/getArticles.js'

export default class ArticleActions extends Actions {

    // Attention : cette action renvoie une promise,
    // il faut donc y souscrire avec de manière asynchrone dans les stores relatifs
    retrieveFromServer(){
        return getArticles()
    }

    // Cette action est réservée aux sockets
    retrieveFromSocket(articles){
        console.log('io', articles)
        return articles
    }

    // move
    move(draggedArticle, currentArticle){
        return {
            draggedArticle: draggedArticle,
            currentArticle: currentArticle
        }
    }


}