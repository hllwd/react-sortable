import { Flummox } from 'flummox'
import ArticleActions from './actions/ArticleActions.js'
import ArticleStore from './stores/ArticleStore.js'
import socket from './io/socket.js'

class Flux extends Flummox {
    constructor(){
        super()
        this.createActions('Article', ArticleActions)
        this.createStore('Article', ArticleStore, this)
    }
}

const flux = new Flux()

// en cas de réception des articles par socket, on appelle l'action de récupération des articles
socket.on('articles', (articles) => {
    flux.getActions('Article').retrieveFromSocket(articles)
})

// on expose un singleton
export default flux