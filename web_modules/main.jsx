import React, {PropTypes} from 'react/addons.js'
import mui from 'material-ui'
import ArticleList from './components/ArticleList.jsx'
import FluxComponent from 'flummox/component.js'
import flux from './dispatcher.js'
import $ from 'jquery'
import socket from './io/socket.js'

let ThemeManager = new mui.Styles.ThemeManager();
let { AppBar } = mui

// notre application : elle gère les composants-controleurs et implémente le theme material ui
// c'est elle qui va souscrire aux stores et va propager ses données aux composants controleurs
class App extends React.Component {

    // vérification du contexte des enfants pour mui
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }

    // vérification des props récupérées du store
    static propTTypes = {
        articles: React.PropTypes.array
    }

    // obligatoire ! https://github.com/acdlite/flummox/issues/61
    // vérification des données du contexte
    static contextTypes = {
        flux: React.PropTypes.any
    }

    //
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }

    // le composant vient d'être rendu dans la dom pour la première fois
    componentWillMount() {
        ThemeManager.setComponentThemes({
            toggle: {
                thumbOnColor: String,
                trackOnColor: String
            }
        });
    }

    render(){

        return(
            <div>
                <AppBar title='Test Scrollable' />
                <ArticleList {...this.props} />
            </div>
        )
    }

}

// on utilise ici le higher order component FluxComponent afin de passer flux dans le context react
$( () => {

    React.render(
        <FluxComponent flux={flux} connectToStores={['Article']}>
            <App />
        </FluxComponent>,
        document.querySelector('body')
    )

})
