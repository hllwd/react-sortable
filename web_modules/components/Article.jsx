import React, {PropTypes} from 'react/addons.js'
import { DragSource, DropTarget } from 'react-dnd'
import mui from 'material-ui'
import flux from '../dispatcher.js'
let { RaisedButton } = mui

let articleSource = {
    beginDrag(props){
        let { article, position } = props
        return {
            article: article
        }
    }
}

let articleTarget = {
    hover(props, monitor, component){
        let draggedArticle = monitor.getItem().article
        let currentArticle = props.article
        if(draggedArticle !== currentArticle){
            flux.getActions('Article').move(draggedArticle, currentArticle)
        }
    }
}

@DropTarget('Article', articleTarget, connect => ({
    connectDropTarget: connect.dropTarget()
}))
@DragSource('Article', articleSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
export default class Article extends React.Component {
    // obligatoire ! https://github.com/acdlite/flummox/issues/61
    // vérification des données du contexte
    static contextTypes = {
        flux: React.PropTypes.any
    }

    render(){
        let { article, isDragging, connectDragSource, connectDropTarget } = this.props
        return connectDragSource(connectDropTarget(
            <li style={{display: 'block'}}>
                <RaisedButton label={article.get('title')} />
            </li>
        ))
    }

}