import React, {PropTypes} from 'react/addons.js'
import HTML5Backend from 'react-dnd/modules/backends/HTML5.js'
import { DragDropContext } from 'react-dnd'
import mui from 'material-ui'
import Article from './Article.jsx'

let {RaisedButton} = mui

@DragDropContext(HTML5Backend)
export default class ArticleList extends React.Component {

    // obligatoire ! https://github.com/acdlite/flummox/issues/61
    // vérification des données du contexte
    static contextTypes = {
        flux: React.PropTypes.any
    }

    constructor(props) {
        super(props)
        this.state = {
            refresh: 0
        }
    }

    // on est automatiquement notifiés dès que des nouvelles props arrivent
    componentWillReceiveProps(nextProps) {
        this.setState({
            refresh: this.state.refresh + 1
        })
    }

    handlerRefresh() {
        this.context.flux.getActions('Article').retrieveFromServer()
    }

    render() {

        let articles = this.props.articles
        let {refresh} = this.state

        return (
            <div>
                <RaisedButton onClick={this.handlerRefresh.bind(this)}>{'refresh  ' + refresh}</RaisedButton>
                <ul>
                    { articles.map((a, k) => <Article
                        key={a.get('id')}
                        article={a}/>)
                    }
                </ul>
            </div>
        )
    }

}